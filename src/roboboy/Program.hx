package roboboy;

import roboboy.Defs;


typedef Program = List<ActionType>;

enum ActionType {
    STOP;
    MOVE (d:Direction);
    USE (i:String, d:Direction);
}

class Action {
    public var type:ActionType;
    public var progress:Float=0;
    
    public function new(type:ActionType) {
        this.type = type;
    }
}
