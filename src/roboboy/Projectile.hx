package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import roboboy.Defs;


class Projectile extends Entity {
    public var appearance:String;
    public var speed:Float=0;
    public var range:Float=0;
    public var moved:Float=0;
    public var dmg:Float=0;
    public var steamroller:Bool=false;
    public var direction:Direction;
    public var _sprite:Image;
    
    public function new(type, dir) {
        super();
        
        var item = Defs.items[type];
        speed = item.speed;
        range = item.range;
        dmg = item.dmg;
        steamroller = item.steamroller;
        
        appearance = type;
        direction = dir;
        load_sprite();
    }
    
    function load_sprite() {
        var item = Defs.items[appearance];
        _sprite = new Image('graphics/items/' + item.bullet + '.png');
        _sprite.centerOrigin();
        switch (direction) {
            case UP: _sprite.angle = 90;
            case LEFT: _sprite.angle = 180;
            case DOWN: _sprite.angle = 270;
            case RIGHT: _sprite.angle = 0;
        }
        graphic = _sprite;
        
        setHitbox(8, 8, Math.floor(x+4), Math.floor(y+4));
        type="projectile";
    }
    
    public override function update() {
        var left_to_move = Math.abs(range*16 - moved);
        var move = Math.min(HXP.elapsed*16*speed, left_to_move);
        if (steamroller) {
            _sprite.angle = Math.floor(moved/(16*4)*360/45)*45;
        }
        moved += move;
        switch(direction) {
            case UP: y -= move;
            case DOWN: y += move;
            case LEFT: x -= move;
            case RIGHT: x += move;
        }
        
        super.update();
        
        if (moved > 16) {
            var col:Array<Character> = [];
            collideInto("character", x, y, col);
            for (c in col) {
                if (c != null && !c.dead) {
                    c.damage(dmg);
                    Sound.play_sound('shoot');
                    if (!steamroller) HXP.scene.remove(this);
                    return;
                }
            }
        }
        if (moved >= range*16) HXP.scene.remove(this);
    }
}
