package roboboy;

import haxe.xml.Fast;
import flash.geom.Point;
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import roboboy.Defs;


enum ItemType {
    SWING;
    SHOOT;
}

class Item {
    public var name:String="";
    public var dmg:Float=1;
    public var range:Int=1;
    public var speed:Int=1;
    public var steamroller:Bool=false;
    public var type:ItemType;
    public var bullet:String;
    
    public static var ITEM_TYPES = [
        'swing'=>SWING,
        'shoot'=>SHOOT
        ];
    
    public function new() {}
    
    public static function parse(fast:Fast):Item {
        var item = new Item();
        if (fast.hasNode.name) item.name = fast.node.name.innerData;
        if (fast.hasNode.damage) item.dmg = Std.parseFloat(fast.node.damage.innerData);
        if (fast.hasNode.type) item.type = ITEM_TYPES[fast.node.type.innerData];
        if (fast.hasNode.range) item.range = Std.parseInt(fast.node.range.innerData);
        if (fast.hasNode.speed) item.speed = Std.parseInt(fast.node.speed.innerData);
        if (fast.hasNode.bullet) item.bullet = fast.node.bullet.innerData;
        if (fast.hasNode.steamroller) item.steamroller = true;
        return item;
    }
}

class Weapon extends Entity {
    public var owner:Character;
    public var item_type:String;
    public var progress:Float = 0;
    public var sprite:Spritemap;
    public var dir:Direction;
    
    public function new(owner:Character, type:String, dir:Direction) {
        super();
        
        this.owner = owner;
        this.item_type = type;
        this.dir = dir;
        
        sprite = new Spritemap('graphics/items/' + type + '.png', 16, 16);
        graphic = sprite;
    }
    
    public static var attack_rotations = [
        UP      =>  [135, 90, 45, 0, 315],
        DOWN    =>  [315, 270, 225, 180, 135],
        RIGHT   =>  [45, 0, 315, 270, 225],
        LEFT    =>  [45, 90, 135, 180, 225]
    ];

    public static var rotation_offsets = [
        0   => new Point(0.707,-0.707),
        45  => new Point(0,-1),
        90  => new Point(-0.707,-0.707),
        135 => new Point(-1,0),
        180 => new Point(-0.707,0.707),
        225 => new Point(0,1),
        270 => new Point(0.707,0.707),
        315 => new Point(1,0)
    ];
    
    public override function update() {
        progress += HXP.elapsed*Defs.SWINGS_PER_SECOND;
        
        var anim = Math.floor(Math.min(progress / (1.0 / 5), 4));
        var angle = attack_rotations[dir][anim];
        var offset = rotation_offsets[angle];
        x = owner.x + (1.2*offset.x)*16;
        y = owner.y + (1.2*offset.y)*16;
        sprite.centerOrigin();
        sprite.angle = angle;
        //if (angle % 10 == 0) sprite.scale = 0.75 else sprite.scale = 1;
        var frame = 
        switch(type) {
            case "umbrella":
                if (anim == 0 || anim == 4) 1 else 0;
            default:
                if (anim == 0 || anim == 4) 0 else 1;
        }
        
        sprite.setFrame(frame, 0);
        
        if (progress >= 1) {
            owner.swing_obj = null;
            HXP.scene.remove(this);
        }
        
        super.update();
    }
}
