package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;


class Controls {
    public static function init() {
        Input.define("dpad", [Key.W, Key.A, Key.S, Key.D, Key.UP, Key.DOWN, Key.LEFT, Key.RIGHT]);
        Input.define("u", [Key.W, Key.UP]);
        Input.define("d", [Key.S, Key.DOWN]);
        Input.define("l", [Key.A, Key.LEFT]);
        Input.define("r", [Key.D, Key.RIGHT]);
        Input.define("a", [Key.L, Key.A]);
        Input.define("b", [Key.K, Key.B]);
        Input.define("start", [Key.ENTER]);
        Input.define("mute", [Key.M]);
        Input.define("fullscreen", [Key.F10]);
    }
    
    private static var thisFrame:Map<String,Bool> = new Map();
    
    public static function pressed(key):Bool {
        if (Input.pressed(key)) return true
        else return (thisFrame.exists(key) && thisFrame[key]);
    }
    
    public static function update() {
        if (!Lambda.empty({"iterator": function() return thisFrame.keys()})) {
            thisFrame = new Map();
        }
        
        if (pressed('swap')) Main.swap_palette();
        if (pressed('mute')) Sound.toggle_mute();
#if desktop
        if (pressed('fullscreen')) HXP.fullscreen = !HXP.fullscreen;
#end
    }
    
    public static function spoof(key) {
        thisFrame[key] = true;
    }
    
    public static function clickedOn(e:Entity) {
        return (Input.mousePressed && 
                e.collidePoint(e.x,e.y,Input.mouseX,Input.mouseY) &&
                e.visible);
    }
    
    public static function clickedOnImage(i:Image) {
        return (Input.mousePressed && 
                i.x <= Input.mouseX && 
                Input.mouseX <= i.x+i.width &&
                i.y <= Input.mouseY &&
                Input.mouseY <= i.y+i.height &&
                i.visible && i.alpha == 1);
    }
}
