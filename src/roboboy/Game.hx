package roboboy;

import haxe.xml.Fast;
import openfl.Assets;


class Game {
    public var current_level:Int=0;
    public var robo:Robo;
    
    public function new() {
    }
    
    public function get_level(lv:Int):Level {
        var _data = Assets.getText('levels/level' + lv + '.xml');
        var _xml = Xml.parse(_data);
        var level = Level.parse(_xml);
        level.robo = new Robo();
        return level;
    }
    
    public function next_level():Level {
        current_level += 1;
        return get_level(current_level);
    }
    
    public function redo_level():Level {
        return get_level(current_level);
    }
}
