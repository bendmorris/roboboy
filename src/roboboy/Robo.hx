package roboboy;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;


class Robo extends Character {
    public function new() {
        super();
        hp_max = 1;
    }
    
    public override function load_sprite() {
        _sprite = new Spritemap('graphics/chars/robo.png', 16, 16);
        
        super.load_sprite();
    }
    
    public function start_level(level:Level) {
        hp = hp_max;
        set_pos(level.startx, level.starty);
        dir = level.startdir;
        
        items = level.items;
        // TODO: what items do you get on this level?
    }
}
