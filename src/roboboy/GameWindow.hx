package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.graphics.Text;
import openfl.Assets;
import roboboy.Defs;
import roboboy.Program;
import roboboy.Enemy;
import roboboy.Controls;


enum Phase {
    Wait;
    Message;
    MoveOrAct;
    ChooseAct;
    ChooseDir;
    Confirm;
    Victory;
    FadeOut;
}

class GameWindow extends GameScene {
    public static inline var MSG_W:Int=9;
    public static inline var MSG_H:Int=3;
    
    public var phase(default, set):Phase;
    function set_phase(phase:Phase) {
        program_buttons.a_visible = phase==MoveOrAct;
        program_buttons.b_visible = phase==MoveOrAct && level.robo.program.length > 0;
        
        action_buttons.a_visible = phase==ChooseAct;
        action_buttons.b_visible = phase==ChooseAct;
        dir_buttons.vis = phase==ChooseDir;
        
        switch (phase) {
            case Message: {
            }
            case ChooseAct: {
                action_buttons.selected = 0;
                //msgBox.set_text('Choose an action with left/right. (' + Defs.B_BTN + ' to cancel)', false);
                var btn_name = action_buttons.btns[action_buttons.selected].val.toUpperCase();
                msgBox.set_text(btn_name + '\n\n(' + Defs.A_BTN + ' to confirm, ' + Defs.B_BTN + ' to cancel)', false);
            }
            case Confirm: {
                Sound.play_sound('confirm');
                msgBox.set_text('Finished? (' + Defs.B_BTN + ' to cancel)');
            }
            case ChooseDir: {
                msgBox.set_text('Choose a direction. (' + Defs.B_BTN + ' to cancel)', false);
            }
            case Victory: {
                msgBox.set_text('Mission complete!');
                Sound.play_sound('confirm');
            }
            default: msgBox.visible = false;
        }
        
        return this.phase = phase;
    }
    
    public var lost = false;
    public var alpha(default,set):Float;
    function set_alpha(n:Float) {
        return bg_grid.alpha = 
               bg_grid.alpha = 
               level.alpha = 
               bg.alpha = 
               round_counter.alpha =
               plan_sign.alpha =
               alpha = n; }
    
    public var level:Level;
    public var game:Game;
    
    public var action_buttons:ActionList;
    public var program_buttons:ButtonList;
    public var dir_buttons:DirList;
    public var bg:Image;
    public var bg_grid:Tilemap;
    public var round_counter:Text;
    public var msgBox:MessageBox;
    public var messageList:List<String>;
    public var whiteOverlay:Image;
    public var plan_sign:Image;
    
    public function new(level:Level, game:Game) {
        super();
        
        bg = new Image("graphics/bg.png");
        addGraphic(bg);
        
        bg_grid = new Tilemap("graphics/tile.png", Defs.PLAY_X*16, Defs.PLAY_Y*16, 16, 16);
        for (a in 0 ... Defs.PLAY_X) {
            for (b in 0 ... Defs.PLAY_Y) {
                bg_grid.setTile(a, b, 0);
            }
        }
        bg_grid.y = 16;
        var e = addGraphic(bg_grid);
                
        this.game = game;
        
        level.robo.start_level(level);
        this.level = level;
        add(level);

        round_counter = new Text("", Defs.WIDTH, 0, 0, 0, Defs.FONT_OPTIONS);
        addGraphic(round_counter);
        set_round_counter();
                
        var actions = [];
        for (item in level.robo.items) {
            actions.push("use " + item);
        }
        actions.push("move");
        actions.push("stop");
        action_buttons = new ActionList(actions.length, 2, 2, actions);
        add(action_buttons);
        
        program_buttons = new ButtonList(level.actions, 2, Defs.HEIGHT - 18);
        add(program_buttons);
        
        dir_buttons = new DirList(4, Math.floor(16*3)-(1.5*8), Math.floor(16*3.5), ["move_l", "move_u", "move_d", "move_r"], 8);
        dir_buttons.vis = false;
        add(dir_buttons);
        
        messageList = level.tutorial;
        //messageList.add('Use the arrows to move, or A to choose an action.');
        
        msgBox = new MessageBox(MSG_W, MSG_H);
        msgBox.x = (Defs.WIDTH - (16*MSG_W))/2;
        msgBox.y = Defs.HEIGHT - 16*(MSG_H+0.5);
        add(msgBox);
        
        next_msg();

        whiteOverlay = Image.createRect(HXP.screen.width, HXP.screen.height, 0xFFFFFF, 0);
        whiteOverlay.alpha = 0;
        addGraphic(whiteOverlay);
        
        plan_sign = new Image("graphics/plan_sign.png");
        plan_sign.x = 4;
        plan_sign.y = Defs.HEIGHT - 20 - plan_sign.height;
        addGraphic(plan_sign);
        
        alpha = 0;
    }
    
    function next_msg(new_msg=null) {
        if (new_msg != null) messageList.add(new_msg);
        var msg_txt = messageList.pop();
        if (msg_txt != null) {
            msgBox.set_text(msg_txt);
            phase = Message;
        } else phase = MoveOrAct;
    }
    
    public override function begin() {
        Sound.play_music('battle');
        
        super.begin();
    }

    function lose() {
        lost = true;
        phase = FadeOut;
    }
    
    public override function update() {
        if (alpha < 1) {
            alpha = Math.min(1, alpha+HXP.elapsed/2.5);
        }

        if (level.robo.dead) {
            lose();
        }
        
        plan_sign.visible = phase==MoveOrAct;
        
        super.update();
        
        phase_update();
    }
    
    function phase_update() {
        switch(phase) {
            case Wait: {
                // see if all actions are finished
                var all_done = true;
                if (level.robo.current_step != null) all_done = false;
                all_done = all_done && 
                           ((Lambda.filter(level.enemies, function(e) { return e.current_step!=null; })).length==0);
                if (all_done) {
                    // does anyone still have actions to complete?
                    var keep_going = execute();
                    if (!keep_going) next_round();
                }
            }
            case Message: {
                if (Controls.pressed('a')) {
                    if (msgBox.additional.length > 0) {
                        msgBox.set_text(msgBox.text + msgBox.additional,
                                        msgBox.show_a, false);
                    } else {
                        next_msg();
                        if (phase == Message) Sound.play_sound('select');
                        else Sound.play_sound('accept');
                    }
                }
            }
            case MoveOrAct: {
                if (Controls.pressed('dpad')) {
                    for (d in Defs.DIRS.keys()) {
                        if (Controls.pressed(d)) {
                            set_step(MOVE(Defs.DIRS[d]), "move_" + d);
                            break;
                        }
                    }
                }
                else if (Controls.pressed('a')) {
                    phase = ChooseAct;
                }
                else if (Controls.pressed('b')) remove_step();
            }
            case ChooseAct: {
                var changed = false;
                if (Controls.pressed('l')) {
                    action_buttons.selected -= 1;
                    if (action_buttons.selected < 0) action_buttons.selected = action_buttons.n-1;
                    changed = true;
                } 
                else if (Controls.pressed('r')) {
                    action_buttons.selected += 1;
                    if (action_buttons.selected >= action_buttons.n) action_buttons.selected = 0;
                    changed = true;
                }
                else if (Controls.pressed('a')) {
                    choose_action();
                }
                else if (Controls.pressed('b')) {
                    action_buttons.selected = -1;
                    phase = MoveOrAct;
                }
                if (changed) {
                    var btn_name = action_buttons.btns[action_buttons.selected].val.toUpperCase();
                    msgBox.set_text(btn_name + '\n\n(' + Defs.A_BTN + ' to confirm, ' + Defs.B_BTN + ' to cancel)', false);
                }
            }
            case ChooseDir: {
                if (Controls.pressed('dpad')) {
                    for (d in Defs.DIRS.keys()) {
                        if (Controls.pressed(d)) {
                            choose_action(d);
                            break;
                        }
                    }
                } else if (Controls.pressed('b')) {
                    phase = ChooseAct;
                }
            }
            case Confirm: {
                if (Controls.pressed('a')) {
                    Sound.play_sound('accept');
                    finish_round();
                }
                else if (Controls.pressed('b')) remove_step();
            }
            case Victory: {
                if (Controls.pressed('a')) {
                    finish_level();
                }
            }
            case FadeOut: {
                alpha = Math.max(0, alpha - HXP.elapsed);
                if (alpha <= 0) {
                    if (lost) {
                        HXP.scene = new GameWindow(game.redo_level(), game);                        
                    } else {
                        if (game.current_level < Defs.levels) {
                            HXP.scene = new GameWindow(game.next_level(), game);
                        } else {
                            HXP.scene = new StartWindow(false);
                        }
                    }
                }
            }
        }
    }
    
    function set_step(s, b) {
        Sound.play_sound('select');
        level.robo.add_step(s);
        program_buttons.set_btn(level.robo.program.length-1, b);
        if (level.robo.program.length >= level.actions) {
            phase = Confirm;
        } else {
            phase = MoveOrAct;
        }
    }
    
    function remove_step() {
        Sound.play_sound('erase');
        level.robo.remove_step();
        program_buttons.set_btn(level.robo.program.length, 'empty');
        phase = MoveOrAct;
    }
    
    function choose_action(d:String=null) {
        var sel_btn = action_buttons.btns[action_buttons.selected];
        if (StringTools.startsWith(sel_btn.val, 'use ')) {
            var item = sel_btn.val.substring(4);
            if (d == null) phase = ChooseDir
            else set_step(USE(item, Defs.DIRS[d]), sel_btn.val);
        } else if (sel_btn.val=='stop') {
            set_step(STOP, 'stop');
        } else if (sel_btn.val=='move') {
            if (d == null) phase = ChooseDir
            else set_step(MOVE(Defs.DIRS[d]), 'move_' + d);
        }
        
        if (phase != ChooseAct && phase != ChooseDir) action_buttons.selected = -1;
    }
    
    function finish_round() {
        phase = Wait;
        execute();
    }
    
    function execute() {
        // have all entities execute their next program step
        // returns true if anyone is doing something
        if (level.robo.program.length > 0) {
            level.robo.execute();
            for (ene in level.enemies) {
                ene.get_program(level);
                ene.execute();
            }
            return true;
        } else {
            // if there are any projectiles, wait for them to finish
            return collideRect('projectile',0,0,Defs.WIDTH,Defs.HEIGHT) != null;
        }
    }
    
    function set_round_counter() {
        round_counter.text = "LEVEL: " + StringTools.lpad("" + game.current_level, " ", 2) + 
                             "\nROUNDS: " + StringTools.lpad("" + level.rounds, " ", 1);
        round_counter.x = Defs.WIDTH - round_counter.textWidth;
    }

    function next_round() {
        program_buttons.empty_btns();
        level.rounds -= 1;
        set_round_counter();
        
        var remaining_enemies = 0;
        for (ene in level.enemies) {
            if (!ene.dead) remaining_enemies += 1;
        }
        for (ene in level.enemies) {
            if (ene.dead) level.enemies.remove(ene);
        }
        
        if (remaining_enemies == 0) {
            phase = Victory;
        } else {
            phase = MoveOrAct;
        }

        if (phase != Victory && level.rounds <= 0) lose();
    }
    
    function finish_level() {
        phase = FadeOut;
    }
}


class ButtonList extends Entity {
    public var btns:Array<Button>;
    public var a_visible=false;
    public var b_visible=false;
    public var selected:Int=-1;
    public var highlight:Image;
    public var n:Int=0;
    public var gap:Float=0;
    public var vis(default,set):Bool;
    function set_vis(v) {
        for (btn in btns) btn.visible = v;
        return vis = v;
    }
    
    public function new(n:Int, x0:Float=0, y0:Float=0, images:Array<String>=null, gap:Float=0) {
        super();
        
        this.n = n;
        this.gap = gap;
        x = x0;
        y = y0;
        
        if (images == null) images = [];
        
        btns = [];
        for (i in 0 ... n) {
            var type = if (i < images.length) images[i] else 'empty';
            var btn = new Button(type);
            btn.x = x0+i*(16+gap);
            btn.y = y0;
            btns.push(btn);
        }
        
        var btn = new Button(Defs.B_BTN);
        btn.x = x0+n*(16+gap);
        btn.y = y0;
        btn.visible=false;
        btns.push(btn);
        
        var btn = new Button(Defs.A_BTN);
        btn.x = x0+(n+1)*(16+gap);
        btn.y = y0;
        btn.visible=false;
        btns.push(btn);
        
        highlight = new Image('graphics/hand.png');
        highlight.x = 2;
        highlight.y = 10;
        highlight.visible=false;
        addGraphic(highlight);
        
        layer=0;
        
        setHitbox(btns.length*16,16);
    }
    
    public override function added() {
        for (btn in btns) HXP.scene.add(btn);
    }
    
    public override function removed() {
        for (btn in btns) HXP.scene.remove(btn);
    }
    
    public function set_btn(n:Int, name:String) {
        btns[n].set_img(name);
    }
    
    public override function update() {
        btns[btns.length-2].visible = b_visible;
        btns[btns.length-1].visible = a_visible;
        
        highlight.visible = (selected > -1);
        if (highlight.visible) {
            var hx = (16+gap)*selected+2;
            if (highlight.x != hx) {
                highlight.x += (hx-highlight.x);
            }
        } else {
            highlight.x = x+2;
        }
        
        if (Controls.clickedOn(this)) {
            var phase = cast(HXP.scene, GameWindow).phase;
            
            if (Controls.clickedOn(btns[btns.length-1])) {
                // a btn
                Controls.spoof('a');
            } else if (Controls.clickedOn(btns[btns.length-2])) {
                // b btn
                Controls.spoof('b');
            }
        }
        
        super.update();
    }
    
    public function empty_btns() {
        for (i in 0...btns.length-2) {
            set_btn(i, 'empty');
        }
    }
}

class ActionList extends ButtonList {
    public override function update() {
        var n = 0;
        if (Controls.clickedOn(this)) {
            var phase = cast(HXP.scene, GameWindow).phase;
            
            if (phase == MoveOrAct || phase == ChooseDir)
                cast(HXP.scene, GameWindow).phase = phase = ChooseAct;
                
            if (phase == ChooseAct) {
                for (btn in btns) {
                    if (Controls.clickedOn(btn)) {
                        if (n < btns.length-2) {
                            selected = n;
                            Controls.spoof('a');
                        }
                    }
                    n += 1;
                }
            }
        }
        
        super.update();
    }
}

class DirList extends ButtonList {
    public override function update() {
        a_visible = b_visible = false;
        
        var n = 0;
        if (Controls.clickedOn(this)) {
            var phase = cast(HXP.scene, GameWindow).phase;
            
            if (phase == ChooseDir) {
                for (btn in btns) {
                    if (Controls.clickedOn(btn)) {
                        if (n < btns.length-2) {
                            Controls.spoof('dpad');
                            Controls.spoof(btn.val.substr(btn.val.length-1, 1));
                        }
                    }
                    n += 1;
                }
            }
        }
        
        super.update();
    }
}

class Button extends Entity {
    public var val:String = "";
    /*public var visible(default,set):Bool;
    function set_visible(v) {
        if (Std.is(graphic, Image)) {
            cast(graphic, Image).visible = v;
        } else if (Std.is(graphic.Spritemap)) {
            cast(graphic, Spritemap).visible = v;
        }
        return visible = v;
    }*/
    
    public function new(type='empty') {
        super();
        set_img(type);
    }
    
    public function set_img(name:String) {
        val = name;
        graphic = null;
        
        if (StringTools.startsWith(name, "use ")) {
            addGraphic(new Image('graphics/btns/empty.png'));
            var item = name.substring(4);
            addGraphic(new Spritemap('graphics/items/' + item + '.png', 16, 16));
        } else {
            addGraphic(new Image('graphics/btns/' + name + '.png'));
        }
        
        setHitbox(16,16);
    }
}

class MessageBox extends GradualText {
    public var a_btn:Image;
    public var show_a=true;
    
    public function new(width:Int, height:Int) {
        var box = new Tilemap("graphics/msg_box.png", 16*width, 16*height, 16, 16);
              
        box.setTile(0, 0, box.getIndex(0, 0));
        box.setTile(width-1, 0, box.getIndex(2, 0));
        box.setTile(0, height-1, box.getIndex(0, 2));
        box.setTile(width-1, height-1, box.getIndex(2, 2));
        if (width > 2) {
            for (x in 1 ... width-1) {
                box.setTile(x, 0, box.getIndex(1, 0));
                box.setTile(x, height-1, box.getIndex(1, 2));
                if (height > 2) {
                    for (y in 1 ... height-1) {
                        box.setTile(x, y, box.getIndex(1,1));
                    }
                }
            }
        }
        if (height > 2) {
            for (y in 1 ... height-1) {
                box.setTile(0, y, box.getIndex(0, 1));
                box.setTile(width-1, y, box.getIndex(2, 1));
            }
        }
        
        addGraphic(box);
        
        super(5, 4, 16*width-10, 16*height-8);
        
        a_btn = new Image('graphics/btns/' + Defs.A_BTN + '.png');
        a_btn.x = (16*width)/2 - 8;
        a_btn.y = (16*height) - 12;
        addGraphic(a_btn);
        
        setHitbox(16*width,16*height+8,0,0);
    }
    
    public override function update() {
        if (show_a && additional.length == 0) {
        flash += HXP.elapsed/0.5;
        if (flash > 1) {
            flash %= 1;
            a_btn.visible = !a_btn.visible;
        }
        } else a_btn.visible = false;
        
        if (Controls.clickedOn(this)) {
            var phase = cast(HXP.scene, GameWindow).phase;
            switch (phase) {
                case ChooseDir, ChooseAct: Controls.spoof('b');
                default: Controls.spoof('a');
            }
        }
        
        super.update();
    }
    
    public override function set_text(txt:String, show_a=true, gradual=true) {
        super.set_text(txt,show_a,gradual);
        
        this.show_a = show_a;
        visible = true;
    }
}

class GradualText extends Entity {
    public var label:Text;
    public var flash:Float=0;
    public var text:String="";
    public var additional:String="";
    public var additional_wait:Float=0;
    public static inline var CHARS_PER_SECOND:Int=30;
    public var alpha(default, set):Float;
    function set_alpha(v) {
        return label.alpha = alpha = v;
    }
    
    public function new(x:Int, y:Int, width:Int, height:Int, color=0x000000) {
        super();
        
        var options = {font:Defs.FONT_NAME, size:Defs.FONT_SIZE, color:color, wordWrap:true};
        label = new Text("", x, y, width, height, options);
        addGraphic(label);
        
        layer = 0;
    }
    
    public function set_text(txt:String, show_a=true, gradual=true) {
        if (gradual) {
            text = label.text = "";
            additional = txt;
        } else {
            text = label.text = txt;
            additional = "";
        }
    }
    
    public override function update() {
        if (additional.length > 0) {
            additional_wait += HXP.elapsed*CHARS_PER_SECOND;
            while (additional_wait >= 1) {
                // add the next character
                var next_char = additional.substr(0,1);
                text += next_char;
                additional = additional.substr(1);
                if (next_char == " " || next_char == "\n") {
                    label.text = text;
                    additional_wait -= 1;
                } else {
                    // pad with nbsps to preserve word wrapping
                    var nbsps = 0;
                    if (!StringTools.isSpace(additional, 0)) {
                        var i = additional.indexOf(' ');
                        if (additional.indexOf('\n') != -1) i = Math.floor(Math.min(i, additional.indexOf('\n')));
                        if (i > -1) nbsps = i
                        else nbsps = additional.length;
                    }
                    label.text = text + StringTools.rpad("", Defs.NBSP, nbsps) + " ";
                    additional_wait -= 1;
                }
            }
        }
        
        super.update();
    }
}
