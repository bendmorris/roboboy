package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import openfl.Assets;
import roboboy.Defs;
import roboboy.Enemy;
import roboboy.GameWindow;


class StartWindow extends GameScene {
    public var controls:Image;
    public var splash:Image;
    public var intro_wait:Float=0;
    public static inline var SPLASH_LENGTH=3;
    public static inline var MOON_LENGTH=30;
    public static inline var HELPER_LENGTH=20;
    public var bg:Image;
    public var logo:Image;
    public var logo2:Image;
    public var logo_wait:Float=0;
    public var press_enter:Float = 0;
    public var press_start_btn:Text;
    public var robo:Robo;
    public var baddie:Enemy;
    public var alpha(default,set):Float;
    function set_alpha(n:Float) {
        return bg.alpha = 
               robo.alpha = 
               baddie.alpha = 
               alpha =
               n;
    }
    
    public var stars1:Image;
    public var stars2:Image;
    public var moon:Image;
    public var intro_text:GradualText;
    public var cur_txt:Int=-1;
    //public var intro_text;
    public var space_alpha(default,set):Float;
    function set_space_alpha(n:Float) {
        moon.alpha = if (n > 0) 1 else 0;
        var c = Math.floor(0xff * n);
        moon.color = c<<16 | c<<8 | c;
        return stars1.alpha = 
               stars2.alpha = 
               intro_text.alpha =
               space_alpha =
               n;
    }
    public static var intro_msgs=[
        "When the invaders attacked, they hit our base on the moon first.",
        "We had no choice but to retreat. Nothing left there now but the service robots.",
        "They're not built to fight, but they can be programmed remotely to follow simple commands.",
        "Program them to grab whatever's nearby and use it! We can't give up!",
    ];
    
    public var transition_out = false;
    
    public function new(show_intro=true) {
        Input.define("start", [Key.ENTER]);
        Input.define("swap", [Key.TAB, Key.SPACE]);
        
        if (!show_intro) intro_wait = 3;
        
        super();
    }
    
    public override function begin() {
        controls = new Image("graphics/controls.png");
        addGraphic(controls);
        
        stars2 = new Image("graphics/stars2.png");
        stars1 = new Image("graphics/stars1.png");
        moon = new Image("graphics/moon.png");
        intro_text = new GradualText(0, 0, Defs.WIDTH, Defs.HEIGHT, 0xffffff);
        addGraphic(stars2);
        addGraphic(stars1);
        addGraphic(moon);
        add(intro_text);
        space_alpha = 0;
        
        splash = new Image("graphics/monsterface.png");
        splash.x = (Defs.WIDTH-splash.width)/2;
        splash.y = (Defs.HEIGHT-splash.height)/2;
        addGraphic(splash);

        bg = new Image("graphics/bg.png");
        addGraphic(bg);
        
        logo = new Image("graphics/logo.png");
        addGraphic(logo);
        
        logo2 = new Image("graphics/logo2.png");
        addGraphic(logo2);
        
        robo = new Robo();
        robo.x = Defs.WIDTH/2;
        robo.y = Defs.HEIGHT-24;
        robo.dir = RIGHT;
        add(robo);
        
        baddie = new HBomb();
        baddie.x = Defs.WIDTH/2;
        baddie.y = Defs.HEIGHT/2+32;
        baddie.dir = LEFT;
        add(baddie);
        
        press_start_btn = new Text("PRESS " + Defs.START_BTN, 0, 0, 0, 0, Defs.FONT_OPTIONS);
        press_start_btn.x = Defs.WIDTH/2 - press_start_btn.textWidth/2;
        press_start_btn.y = Defs.HEIGHT-16;
        addGraphic(press_start_btn);
        
        alpha = 0.33;
        
        super.begin();
    }
    
    public override function update() {
        var v = press_start_btn.visible;
        press_start_btn.visible = true;
        if (Controls.clickedOnImage(press_start_btn))
            Controls.spoof('start');
        press_start_btn.visible = v;
        
        if (intro_wait < 1) {
            logo.alpha = logo2.alpha = controls.alpha = alpha = 0;
            splash.alpha = if (intro_wait < 0.25) intro_wait/0.25 else 
                           if (intro_wait > 0.75) (1-intro_wait)/0.25
                           else 1;
            intro_wait += HXP.elapsed/SPLASH_LENGTH;
            if (Controls.pressed('start')) intro_wait = 1;
        } else if (intro_wait < 2) {
            splash.alpha = 0;
            logo.alpha = logo2.alpha = splash.alpha = alpha = 0;
            controls.alpha = 1;
            intro_wait += HXP.elapsed/HELPER_LENGTH;
            if (Controls.pressed('start')) intro_wait = 2;
        } else if (intro_wait < 3) {
            if (Sound.music_playing != "battle") Sound.play_music('battle');
            controls.alpha = 0;
            var prog = intro_wait - 2;
            var txtn = Math.floor(Math.min(Math.floor((prog-0.1)/0.9 * intro_msgs.length), intro_msgs.length-1));
            if (txtn >= 0 && cur_txt != txtn) {
                intro_text.set_text(intro_msgs[txtn]);
                cur_txt = txtn;
            }
            space_alpha = if (prog < 0.1) prog/0.1 else 
                          if (prog > 0.9) (1-prog)/0.1
                          else 1;
            stars1.y = 0-(72*((prog%0.5)/0.5));
            stars2.y = 0-(72*(prog));
            moon.y = 0 + 16*prog;
            intro_wait += HXP.elapsed/MOON_LENGTH;
            if (Controls.pressed('start')) intro_wait = 3;
        } else if (transition_out) {
            alpha = logo.alpha = logo2.alpha = Math.max(0, alpha-HXP.elapsed/2.5);
            if (alpha == 0) {
                transition_out = false;
                next();
            }
        } else {
            space_alpha = controls.alpha = splash.alpha = 0;
            
            if (Sound.music_playing != "theme") Sound.play_music('theme');
            
            if (alpha < 1) alpha = Math.min(1, alpha+HXP.elapsed/2.5);
            if (logo.alpha < 1) logo.alpha = logo2.alpha = Math.min(1, logo.alpha+HXP.elapsed/(3+logo.alpha*3));
            else {
                logo_wait += HXP.elapsed;
                if (logo_wait >= 1) {
                    logo_wait = 0;
                    logo.alpha = logo2.alpha = 0.4;
                }
            }
            
            var move = HXP.elapsed*16*Defs.TILES_PER_SECOND;
            if (robo.dir == RIGHT) {
                robo.x += move;
                if (robo.x >= Defs.WIDTH-16) {
                    robo.x = Defs.WIDTH-16;
                    robo.dir = LEFT;
                }
            } else {
                robo.x -= move;
                if (robo.x <= 16) {
                    robo.x = 16;
                    robo.dir = RIGHT;
                }
            }
            if (baddie.dir == RIGHT) {
                baddie.x += move*2;
                baddie.step += HXP.elapsed/Defs.STEP_RATE;
                if (baddie.x >= Defs.WIDTH-16) {
                    baddie.x = Defs.WIDTH-16;
                    baddie.dir = LEFT;
                }
            } else {
                baddie.x -= move*2;
                baddie.step += HXP.elapsed/Defs.STEP_RATE;
                if (baddie.x <= 16) {
                    baddie.x = 16;
                    baddie.dir = RIGHT;
                }
            }

            if (Controls.pressed("start")) {
                Sound.play_sound('accept');
                transition_out = true;
            }
        }
        
        if (!transition_out && intro_wait>1) {
            press_enter += HXP.elapsed;
        
            if (press_start_btn != null) {
                if (press_enter > 1) {
                    press_start_btn.visible = !(press_start_btn.visible);
                    press_enter -= 1;
                }
            }
        } else press_start_btn.visible = false;
        
        super.update();
    }
    
    function next() {
        HXP.scene = new IntroWindow();
    }
}
