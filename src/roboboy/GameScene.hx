package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.graphics.Image;


class GameScene extends Scene {
#if mobile
    public var mute_btn:Entity;
#end
    
    public function new() {
        super();

        var base = Image.createRect(Defs.WIDTH, Defs.HEIGHT, 0xFFFFFF, 1);
        base.scrollX = base.scrollY = 0;
        //base.layer = HXP.BASELAYER;
        addGraphic(base);
    }
    
    public override function begin() {
#if mobile
        var mute_lbl = new Text('mute', 0, 0, 0, 0, Defs.FONT_OPTIONS);
        mute_btn = new Entity(
            Defs.WIDTH-mute_lbl.textWidth,
            Defs.HEIGHT-mute_lbl.textHeight,
            mute_lbl);
        mute_btn.setHitbox(mute_lbl.textWidth,mute_lbl.textHeight);
        add(mute_btn);
#end
    }
    
    public override function update() {
        Controls.update();
        
#if mobile
        if (Controls.clickedOn(mute_btn)) {
            Sound.toggle_mute();
        }
#end
        
        super.update();
    }
}
