package roboboy;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import roboboy.Defs;



class Enemy extends Character {
    public function get_program(lv:Level) {
        // by default, stand there and do nothing
    }
}

class Target extends Enemy {
    public function new() {
        super();
    }
    
    public override function load_sprite() {
        _sprite = new Spritemap('graphics/chars/target.png', 16, 16);
        
        super.load_sprite();
    }
}

class Bomb extends Enemy {
    public var pref_dir:String;
    
    public override function load_sprite() {
        _sprite = new Spritemap('graphics/chars/bomb_' + pref_dir + '.png', 16, 16);
        
        super.load_sprite();
    }
    
    public override function get_program(lv:Level) {
        var non_pref_dir = if (pref_dir == 'h') 'v'
                           else 'h';
        for (d in [pref_dir, non_pref_dir]) {
            if (d == 'h') {
                if (x > lv.robo.x) {
                    program.add(MOVE(LEFT));
                    return;
                } else if (x < lv.robo.x) {
                    program.add(MOVE(RIGHT));
                    return;
                }
            } else if (d == 'v') {
                if (y > lv.robo.y) {
                    program.add(MOVE(UP));
                    return;
                } else if (y < lv.robo.y) {
                    program.add(MOVE(DOWN));
                    return;
                }
            }
        }
    }
}
class HBomb extends Bomb {
    public function new() {
        pref_dir='h';
        super();
    }
}
class VBomb extends Bomb {
    public function new() {
        pref_dir='v';
        super();
    }
}

class Carrot extends Enemy {
    public function new() {
        super();
        hp = 2;
    }
    
    public override function load_sprite() {
        _sprite = new Spritemap('graphics/chars/target.png', 16, 16);
        
        super.load_sprite();
    }
}


class Enemies {
    public static inline function enemy_type(name:String):Enemy {
        return switch(name) {
            case "target": new Target();
            case "hbomb": new HBomb();
            case "vbomb": new VBomb();
            case "carrot": new Carrot();
            default: new Enemy();
        }
    }
}
