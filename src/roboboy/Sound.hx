package roboboy;

import com.haxepunk.Sfx;


class Sound {
    public static var muted:Bool = false;

    public static var audio:Map<String, Sfx>;
    public static var sound_loaded=false;
    // preload sound effects and music
    public static var SOUNDS=["shoot","dead","swing","accept","erase","confirm","collide"];
    public static var MUSIC=["theme","battle"];
#if flash
    public static inline var sound_suffix=".mp3";
    public static inline var music_suffix=".mp3";
#else
    public static inline var sound_suffix=".wav";
    public static inline var music_suffix=".ogg";
#end
    public static var music_playing="";
    
    public static var volume(get,never):Float;
    static function get_volume() {
        return if (muted) 0 else 1;
    }
    
    public static function load_sounds() {
        if (sound_loaded) return;
        
        sound_loaded = true;
        audio = new Map<String, Sfx>();
        
        for (file in SOUNDS) {
            audio[file] = new Sfx("sound/" + file + sound_suffix);
            audio[file].type = 'sound';
        }
        
        for (file in MUSIC) {
            audio[file] = new Sfx("music/" + file + music_suffix);
            audio[file].type = 'music';
        }
        
        play_sound('confirm');
    }
    
    public static function play_sound(name) {
        if (!audio.exists(name)) {
            audio[name] = new Sfx("sound/" + name + sound_suffix);
            audio[name].type = 'sound';
        }
        
        audio[name].play();
    }
    
    public static function play_music(name, loop=true, force_restart=false) {
        if (force_restart || music_playing != name) {
            stop_music();
            if (!audio.exists(name)) {
                audio[name] = new Sfx("music/" + name + music_suffix);
                audio[name].type = "music";
            }
            
            music_playing = name;
            if (loop) audio[name].loop();
            else audio[name].play();
            
            if (muted) audio[name].stop();
            //player.loadSong(Assets.getBytes("music/" + name + ".xm"));
        }
    }
    
    public static function stop_music(erase=true) {
        if (music_playing != "") {
            if (audio.exists(music_playing))
                audio[music_playing].stop();
            //player.stop();
            if (erase) music_playing = "";
        }
    }
    
    public static function toggle_mute() {
        muted = !muted;
        Sfx.setVolume('sound', volume);
        if (music_playing != "") {
            if (muted) {
                audio[music_playing].stop();
            } else {
                audio[music_playing].resume();
            }
        }
    }
}
