package roboboy;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import openfl.Assets;
import roboboy.Defs;
import roboboy.Enemy;
import roboboy.GameWindow;


class IntroWindow extends GameScene {
    public var robo:Robo;
    public var baddie:Enemy;
    public var progress:Float = 0.5;
    public static inline var transition_time=1.5;
    public var scene:Int = 0;
    public var good_sign:Image;
    public var bad_sign:Image;
    public var hand:Image;
    public var alpha(default,set):Float;
    public var transition_out = false;
    public var images:Array<Image>;
    function set_alpha(n:Float) {
        for (g in images) g.alpha = n;
        if (good_sign.alpha > n) good_sign.alpha = n;
        if (bad_sign.alpha > n) bad_sign.alpha = n;
        return robo.alpha = 
               baddie.alpha = 
               alpha =
               n;
    }
    
    public function new() {
        super();
        images = [];
    }
    
    public override function begin() {
        robo = new Robo();
        robo.x = Defs.WIDTH*0.25;
        robo.y = Defs.HEIGHT/2 - 16;
        robo.dir = DOWN;
        add(robo);
        good_sign = new Image("graphics/good_sign.png");
        good_sign.x = robo.x - 4;
        good_sign.y = robo.y - good_sign.height - 16;
        addGraphic(good_sign);
        
        baddie = new HBomb();
        baddie.x = Defs.WIDTH*0.75;
        baddie.y = Defs.HEIGHT/2 - 16;
        baddie.dir = DOWN;
        add(baddie);
        bad_sign = new Image("graphics/bad_sign.png");
        bad_sign.x = baddie.x - bad_sign.width + 8;
        bad_sign.y = baddie.y + 16;
        bad_sign.alpha = 0;
        addGraphic(bad_sign);
        
        alpha = 1;
    }
    
    function setScene(n:Int) {
        scene = n;
        switch(n) {
            case 2: {bad_sign.alpha = 1;}
            case 4: {
                good_sign.alpha = bad_sign.alpha = 0;
                
                var label = new Image("graphics/plan_sign.png");
                label.x = 4;
                label.y = Defs.HEIGHT - 20 - label.height;
                addGraphic(label);

                var btn1 = new Image("graphics/btns/empty.png");
                btn1.x = 4;
                btn1.y = 4;
                addGraphic(btn1);
                var btn2 = new Spritemap("graphics/items/sword.png", 16, 16);
                btn2.x = 4;
                btn2.y = 4;
                addGraphic(btn2);
                
                var rightarrow = new Image("graphics/btns/move_r.png");
                rightarrow.x = 4;
                rightarrow.y = Defs.HEIGHT - 20;
                addGraphic(rightarrow);
                
                images.push(btn1);
                images.push(btn2);
                images.push(rightarrow);
                images.push(label);
            }
            case 5: {
                var i = images.pop();
                
                var rightarrow = new Image("graphics/btns/move_r.png");
                rightarrow.x = 4+16;
                rightarrow.y = Defs.HEIGHT - 20;
                addGraphic(rightarrow);
                images.push(rightarrow);
                images.push(i);
            }
            case 6: {
                images.pop().alpha = 0;
                
                hand = new Spritemap("graphics/hand.png", 16, 16);
                hand.x = 4;
                hand.y = 4+8;
                addGraphic(hand);
                
                var btn = new Image("graphics/btns/" + Defs.A_BTN + ".png");
                btn.x = 4+16*2;
                btn.y = Defs.HEIGHT - 20;
                addGraphic(btn);
                images.push(btn);
            }
            case 7: {
                hand.alpha = 0;
                images.pop().alpha = 0;
                
                var btn1 = new Image("graphics/btns/empty.png");
                btn1.x = 4+16*2;
                btn1.y = Defs.HEIGHT - 20;
                addGraphic(btn1);
                var btn2 = new Spritemap("graphics/items/sword.png", 16, 16);
                btn2.x = 4+16*2;
                btn2.y = Defs.HEIGHT - 20;
                addGraphic(btn2);
                images.push(btn1);
                images.push(btn2);
            }
            case 8: {
                var btn = new Image("graphics/btns/" + Defs.A_BTN + ".png");
                btn.x = 4+16*4;
                btn.y = Defs.HEIGHT - 20;
                addGraphic(btn);
                images.push(btn);

                robo.program.add(MOVE(RIGHT));
                robo.program.add(MOVE(RIGHT));
                robo.program.add(USE("sword", RIGHT));
                baddie.program.add(MOVE(LEFT));
                baddie.program.add(MOVE(LEFT));
            }
            case 10: {
                transition_out = true;
            }
            default: {}
        }
    }
    
    public override function update() {
        if (robo.program != null && robo.program.length > 0) {
            if (robo.current_step == null &&
                baddie.current_step == null) {
                robo.execute();
                baddie.execute();
            }
        } else {
            progress += HXP.elapsed/transition_time;
            if (scene >= 3 && scene <= 7) progress += HXP.elapsed/transition_time;
            if (progress > scene) setScene(scene+1);
        }
        
        if (transition_out) {
            alpha = Math.max(0, alpha - HXP.elapsed*2);
            if (alpha <= 0) next();
        }
        
        super.update();
        
        if (Input.mousePressed || Controls.pressed('a')) {
            progress = scene;
            if (progress == 3 || progress == 9) progress += 1;
        }
        if (Controls.pressed('start')) transition_out = true;
    }
    
    function next() {
        var game = new Game();
        var level = game.next_level();
        HXP.scene = new GameWindow(level, game);
    }
}
