package roboboy;

import haxe.xml.Fast;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import openfl.Assets;


enum Direction {
    UP;
    DOWN;
    LEFT;
    RIGHT;
}

class Defs {
    public static var PALETTES=[
        [0x262626, 0x525252, 0xa5a5a5, 0xf3f3f3],
        [0x393829, 0x7b7163, 0xb5a66b, 0xe7d79c],
        [0x262626, 0x833100, 0xffad63, 0xf3f3f3],
        [0x262626, 0x008300, 0x7bff30, 0xf3f3f3],
        [0x262626, 0x833100, 0xff8584, 0xf3f3f3],
        [0x262626, 0x833100, 0xffad63, 0xf3f3f3],
        [0x262626, 0x9394fe, 0xfe9494, 0xf3f3f3],
        [0x262626, 0x0000fe, 0x65a49b, 0xf3f3f3],
        [0x262626, 0x833100, 0xffad63, 0xf3f3f3],
        [0x262626, 0xff4200, 0x51ff00, 0xf3f3f3],
        [0x262626, 0x943a3a, 0xff8584, 0xf3f3f3],
        [0xf3f3f3, 0xffde00, 0x008486, 0x262626]
        ];
    public static inline var SCALE=3;
    public static inline var WIDTH=160;
    public static inline var HEIGHT=144;
    public static inline var PLAY_X=10;
    public static inline var PLAY_Y=7;
    public static var levels:Int=0;
    
    public static var NBSP='_';//String.fromCharCode(160);
    public static inline var FONT_NAME='font/prstartk.ttf';
    public static inline var FONT_SIZE=8;
    public static var FONT_COLOR=0x262626;
    public static var FONT_OPTIONS={font:FONT_NAME, size:FONT_SIZE, color:FONT_COLOR};
#if (flash || desktop)
    public static var START_BTN="ENTER";
    public static var A_BTN="L";
    public static var B_BTN="K";
#else
    public static var START_BTN="sTART";
    public static var A_BTN="A";
    public static var B_BTN="B";
#end    
    public static inline var STEP_RATE=0.2;
    public static inline var TILES_PER_SECOND=2;
    public static inline var SWINGS_PER_SECOND=3.5;
    
    public static var DIRS = ['u'=>UP, 'd'=>DOWN, 'l'=>LEFT, 'r'=>RIGHT];
    
    public static var items:Map<String, Item>;
    
    public static inline function dir_int(d:Direction) {
        return Type.enumIndex(d);
    }
    
    public static function init() {
        Controls.init();
        
        // count levels
        while (Assets.exists('levels/level' + (levels+1) + '.xml')) levels += 1;
        
        load_items();
        Sound.load_sounds();
    }
    
    static function load_items() {
        items = new Map<String, Item>();
        var _data = Assets.getText('data/items.xml');
        var _xml = Xml.parse(_data);
        var fast = new Fast(_xml.firstElement());
        for (item in fast.nodes.item) {
            var this_item = Item.parse(item);
            items[this_item.name] = this_item;
        }
    }
}
