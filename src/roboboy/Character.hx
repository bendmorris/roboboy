package roboboy;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import roboboy.Defs;
import roboboy.Program;
import roboboy.Item;


class Character extends Entity {
    public var hp_max:Int = 1;
    public var hp:Float=0;
    public var step:Float=0;
    public var dir:Direction;
    
    public var px(get,set):Float;
    function get_px() { return (x+8)/16; }
    function set_px(n:Float) { return x = n*16-8; }
    public var py(get,set):Float;
    function get_py() { return (y-8)/16; }
    function set_py(n:Float) { return y = n*16+8; }
    
    public function set_pos(px, py) { this.px = px; this.py = py; }
    
    public var alpha(get,set):Float;
    function get_alpha() { return _sprite.alpha; }
    function set_alpha(n:Float) {
        if (dead && n > alpha) n = 0;
        return _sprite.alpha = n;
    }
    
    private var _sprite:Spritemap;
    private var _frame:Int=-1;
    
    public var program:Program;
    public var current_step:Action;
    
    public var items:Array<String>;
    
    public var swing_obj:Entity;
    
    public var dead(get,never):Bool;
    function get_dead(){ return hp <= 0; }
    
    public function new() {
        super();
        
        dir = Direction.DOWN;
        load_sprite();
        hp = hp_max;
        
        program = new Program();
        items = [];
    }
    
    public function load_sprite() {
        _sprite.centerOrigin();
        graphic = _sprite;
        
        setHitbox(16, 16, Math.floor(x+8), Math.floor(y+8));
        type="character";
    }
    
    public override function update() {
        if (dead) {
            if (alpha > 0) alpha = Math.max(alpha-HXP.elapsed, 0);
        } else {
            step += HXP.elapsed/Defs.STEP_RATE;
            step %= 2;
            
            var fy = Defs.dir_int(dir);
            var fx = if (step < 1) 0 else 1;
            var newFrame = _sprite.getFrame(fx, fy);
            if (newFrame != _frame) {
                _sprite.setFrame(fx, fy);
            }
            
            // execute the current action
            if (current_step != null) act();
        }
        
        super.update();
    }
    
    public function act() {
        switch(current_step.type) {
            case STOP:
                // stand still for the time it would take to move once
                current_step.progress += HXP.elapsed*Defs.TILES_PER_SECOND;
                if (current_step.progress >= 1) current_step = null;
                
            case MOVE(d): {
                // move one space
                if (current_step.progress < 1) {
                    dir = d;
                    
                    var left_to_move = 1-current_step.progress;
                    var moved = Math.min(left_to_move, HXP.elapsed*Defs.TILES_PER_SECOND);
                    current_step.progress += moved;
                    switch(d) {
                        case UP: y -= moved*16;
                        case DOWN: y += moved*16;
                        case LEFT: x -= moved*16;
                        case RIGHT: x += moved*16;
                    }
                    if (px < 1) px = 1;
                    if (px > Defs.PLAY_X) px = Defs.PLAY_X;
                    if (py < 1) py = 1;
                    if (py > Defs.PLAY_Y) py = Defs.PLAY_Y;
                } else {
                    x = Math.round(x/8)*8;
                    y = Math.round(y/8)*8;
                    current_step = null;
                    check_collision();
                }
            }
            
            case USE(i, d): {
                dir = d;
                
                // use an item
                var item = Defs.items[i];
                
                if (current_step.progress == 0) {
                    // start using
                    switch(item.type) {
                        case SWING: {
                            Sound.play_sound('swing');
                            swing_obj = new Weapon(this, item.name, d);
                            HXP.scene.add(swing_obj);
                        }
                        case SHOOT: {
                            Sound.play_sound('shoot');
                            var proj = new Projectile(item.name, d);
                            proj.x = x;
                            proj.y = y;
                            HXP.scene.add(proj);
                        }
                    }
                }
                
                current_step.progress += HXP.elapsed*Defs.TILES_PER_SECOND;
                
                if (current_step.progress >= 1) {
                    if (item.type == SWING) {
                        var rotations = Weapon.attack_rotations[d];
                        var offsets = [for (r in rotations) Weapon.rotation_offsets[r]];
                        for (offset in offsets) {
                            var rx = if (offset.x > 0) 1 else if (offset.x < 0) -1 else 0;
                            var ry = if (offset.y > 0) 1 else if (offset.y < 0) -1 else 0;
                            var col:Character = cast(collide("character", x+rx*16, y+ry*16), Character);
                            if (col != null) {
                                col.damage(item.dmg);
                            }
                        }
                    }
                    
                    current_step = null;
                }
            }
        }
    }
    
    public function check_collision() {
        var col:Array<Character> = [];
        collideInto("character", x, y, col);
        for (c in col) {
            if (!c.dead && distanceFrom(c) < 12) {
                Sound.play_sound('collide');
                damage(1);
                c.damage(1);
            }
        }
    }
    
    public function damage(dmg:Float) {
        if (!dead) {
            hp -= dmg;
            if (hp <= 0) {
                hp = 0;
                die();
            }
        }
    }
    
    public function die() {
        current_step = null;
        Sound.play_sound('dead');
    }
    
    public function execute() {
        if (dead) return;
        // start executing the next programmed instruction
        var t = program.pop();
        current_step = if (t == null) null else new Action(t);
    }
    
    public function add_step(step:ActionType) {
        if (program == null) program = new Program();
        program.add(step);
    }
    
    public function remove_step() {
        program = Lambda.list(Lambda.array(program).slice(0, program.length-1));
    }
}
