package roboboy;

import haxe.xml.Fast;
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import roboboy.Defs;
import roboboy.Enemy;


class Level extends Entity {
    public var robo:Robo;
    public var enemies:Array<Enemy>;
    
    public var startx:Int=3;
    public var starty:Int=4;
    public var startdir:Direction;
    public var items:Array<String>;
    public var qty:Map<String, Int>;
    
    public var rounds:Int=2;
    public var actions:Int=4;
    
    public var tutorial:List<String>;
    
    public var alpha(default, set):Float;
    function set_alpha(n:Float) {
        robo.alpha = n;
        for (enemy in enemies) {
            enemy.alpha = n;
        }
        return alpha=n;
    }
    
    public function new() {
        super();
        enemies = [];
        items = [];
        qty = new Map<String, Int>();
        tutorial = new List<String>();
    }
    
    public override function added() {
        HXP.scene.add(robo);
        for (enemy in enemies) {
            HXP.scene.add(enemy);
        }
    }
    
    public override function removed() {
        HXP.scene.remove(robo);
        for (enemy in enemies) {
            HXP.scene.remove(enemy);
        }
    }
    
    public static inline function parse(xml:Xml):Level {
        var fast = new Fast(xml.firstElement());
        
        var level = new Level();
        
        if (fast.hasNode.rounds)
            level.rounds = Std.parseInt(fast.node.rounds.innerData);
        if (fast.hasNode.actions)
            level.actions = Std.parseInt(fast.node.actions.innerData);
        
        for (item in fast.nodes.item) {
            level.items.push(item.innerData);
            var q = if (item.has.qty) Std.parseInt(item.att.qty)
                    else -1;
            if (q > 0) {
                level.qty[item.innerData] = q;
            }
        }
        
        for (msg in fast.nodes.msg) {
            level.tutorial.add(msg.innerData);
        }
        
        if (fast.hasNode.startx)
            level.startx = Std.parseInt(fast.node.startx.innerData);
        if (fast.hasNode.starty)
            level.starty = Std.parseInt(fast.node.starty.innerData);
        if (fast.hasNode.startdir)
            level.startdir = Defs.DIRS.get(fast.node.startdir.innerData);
        else level.startdir = RIGHT;
        for (ene in fast.nodes.enemy) {
            var type = ene.att.type;
            var enemy = Enemies.enemy_type(type);
            var x = Std.parseInt(ene.att.x);
            var y = Std.parseInt(ene.att.y);
            enemy.set_pos(x, y);
            if (ene.has.dir) 
                enemy.dir = Defs.DIRS.get(ene.att.dir);
            else enemy.dir = DOWN;
            
            level.enemies.push(enemy);
        }
        
        return level;
    }
}
