import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.filters.ColorMatrixFilter;
import flash.geom.Rectangle;
import flash.geom.Point;
import flash.events.Event;
import flash.Lib;
import openfl.Assets;
import com.haxepunk.HXP;
import com.haxepunk.Engine;
import com.haxepunk.RenderMode;
import com.haxepunk.utils.Draw;
import roboboy.Defs;


class Main extends Engine {
    public static var palette:Int = 0;
    public static var rArray:Array<Int>;
    public static var gArray:Array<Int>;
    public static var bArray:Array<Int>;
    public static var matrixFilter:ColorMatrixFilter;
    public static var scanlines:Bitmap;
    public static inline var SCANLINE_COLOR=0x303030;
    public static inline var SCANLINE_ALPHA=0.2;
    public static inline var RGB_NOISE=4;
    public var sprite(get,never):Sprite;
    function get_sprite() return HXP.scene.sprite;

    public function new(width:Int = 0, height:Int = 0, frameRate:Float = 30, fixed:Bool = false, ?renderMode:RenderMode) {
        super(width, height, frameRate, fixed, RenderMode.BUFFER);
        //super(width, height, frameRate, fixed, renderMode);
        cacheAsBitmap = true;
    }

    override public function init() {
        load_colors();
#if flash
        var b = 0.33;
        var c = 0.33;
#else
        var b = 0.33;
        var c = 0.33;
#end
        var matrix = [c, b, b, 0, 0, 
                      b, c, b, 0, 0, 
                      b, b, c, 0, 0, 
                      0, 0, 0, 1, 0];
        matrixFilter = new ColorMatrixFilter(matrix);
        
#if debug
        HXP.console.enable();
#end
        HXP.screen.scaleX = HXP.screen.scaleY = Defs.SCALE;
        HXP.screen.color = 0xFFFFFF;
        HXP.screen.smoothing = false;
        
        HXP.stage.quality = flash.display.StageQuality.LOW;
        
        Defs.init();
        
        HXP.scene = new roboboy.StartWindow();
        onResize();

        HXP.stage.addEventListener(Event.RESIZE, onResize);
    }
    
    public static function main() { new Main(); }

    public function onResize(event=null) {
        if (scanlines != null) sprite.removeChild(scanlines);

        var scanlines_bmd = new BitmapData(Math.floor(width), Math.floor(height), true, 0);
        var scanline_freq = Defs.SCALE;
        scanlines_bmd.noise(1, 0, RGB_NOISE);
        Draw.setTarget(scanlines_bmd);
        for (i in 0...Math.floor(height/scanline_freq)) {
            var yi = Math.floor(i * scanline_freq);
            Draw.linePlus(0, yi, Math.floor(width), yi, SCANLINE_COLOR, SCANLINE_ALPHA);
        }
        scanlines = new Bitmap(scanlines_bmd);
    }
    
    static inline function greyscale(buffer:BitmapData) {
        buffer.applyFilter(buffer, new Rectangle(0,0,Defs.WIDTH,Defs.HEIGHT),new Point(0,0),matrixFilter);
    }
    
    static inline function rgbToHex(r:Int, g:Int, b:Int) {
        return (r << 16 | g << 8 | b);
    }
    
    public static inline function paletteMap(buffer:BitmapData) {
        buffer.paletteMap(buffer, buffer.rect, new Point(0,0), 
                          rArray, gArray, bArray);
    }
    
    static inline function load_colors() {
        var colors = Defs.PALETTES[palette];
        rArray = [];
        gArray = [];
        bArray = [];
        for (n in 0...colors.length) {
            var color:Int = colors[n];
            var r:Int = ((color >> 16) & 255);
            var g:Int = ((color >> 8) & 255);
            var b:Int = (color & 255);
            rArray = rArray.concat([for (i in 64*n ... 64*(n+1)) r<<16]);
            gArray = gArray.concat([for (i in 64*n ... 64*(n+1)) g<<8]);
            bArray = bArray.concat([for (i in 64*n ... 64*(n+1)) b]);
        }
    }

    public static inline function swap_palette() {
        Main.palette = (Main.palette + 1) % Defs.PALETTES.length;
        Main.load_colors();
    }
    
    public override function render() {
        super.render();
        
        greyscale(HXP.buffer);
        paletteMap(HXP.buffer);

        var sprite = HXP.scene.sprite;
        if (!sprite.contains(scanlines)) {
            sprite.addChild(scanlines);
            sprite.blendMode = flash.display.BlendMode.ADD;
        }
    }
}
