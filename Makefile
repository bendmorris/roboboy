name=roboboy
all: assets flash
.PHONY:all assets

sounds: $(patsubst assets/sound/%.wav, assets/sound/%.mp3, $(wildcard assets/sound/*.wav))
music: $(patsubst assets/music/%.xm, assets/music/%.mp3, $(wildcard assets/music/*.xm)) \
       $(patsubst assets/music/%.xm, assets/music/%.ogg, $(wildcard assets/music/*.xm))

%.wav: %.xm
	xmp $< -o $@
%.mp3: %.wav
	rm -f $@
	ffmpeg -i $< -acodec libmp3lame $@
%.ogg: %.wav
	rm -f $@
	ffmpeg -i $< -acodec libvorbis $@

assets: sounds music

flash: assets
	haxelib run openfl build flash -v
	chromium flash.html
flash-debug: assets
	haxelib run openfl build flash -debug -v
	chromium flash.html

linux: assets
	haxelib run openfl test linux -v
linux-debug: assets
	haxelib run openfl test linux -debug -v

windows: assets
	haxelib run openfl test windows -v
windows-debug: assets
	haxelib run openfl test windows -debug

mac: assets
	haxelib run openfl test mac -v
mac-debug: assets
	haxelib run openfl test mac -debug -v

android: assets android-keystore
	haxelib run openfl build android -v
android-debug: assets
	haxelib run openfl build android -debug -v
android-install: android
	adb uninstall com.monsterface.$(name)
	adb install bin/android/bin/bin/$(name)-release.apk
android-keystore: $(name).keystore
$(name).keystore:
	keytool -genkey -v -keystore "$(name).keystore" -alias $(name) -keyalg RSA -keysize 2048 -validity 10000

neko:
	haxelib run openfl test neko -v
neko-debug:
	haxelib run openfl test neko -debug -v

html:
	haxelib run openfl test html5 -v
html-debug:
	haxelib run openfl test html5 -debug -v
